# varfmt

Реализуйте функцию `varfmt.Sprintf`. Функция принимает формат строку и переменное число аргументов.

Синтаксис формат-строки похож формат-строки питона:
 - `{}` - задаёт ссылку на аргумент
 - `{number}` - ссылается на агрумент с индексом `number`
 - `{}` ссылается на аргумент с индексом равным позиции `{}` внутри паттерна

Например, `varfmt.Sprintf("{1} {0}", "Hello", "World)` должен вернуть строку `Hello World`.

Аргументы функции могут быть произвольными типами. Вам нужно форматировать их так же, как это
делает функция `fmt.Sprint`. Вызывать `fmt.Sprint` для форматирования отдельного аргумента
не запрещается.

Ваше решение будет сравниваться с baseline-решением на бенчмарке. Ваш код должен
быть не более чем в два раза хуже чем baseline.